<?php

namespace Wpify\Orm\Abstracts;

use PmsDeps\Wpify\Model\Abstracts\AbstractRepository;
use Wpify\Orm\ORM;

/**
 * Class OrmCenterRepository
 */
abstract class AbstractOrmRepository extends AbstractRepository {
	private ORM $orm;
	private $table;

	/**
	 * AbstractOrmRepository constructor.
	 *
	 * @param ORM $orm
	 *
	 * @throws \ReflectionException
	 */
	public function __construct( ORM $orm ) {
		$c           = new \ReflectionClass( $this->model() );
		$this->table = $c->getConstant( 'TABLE' );
		$this->orm   = $orm;
	}

	public function all() {
		$result = [];
		foreach ( $this->orm->all( $this->table ) as $item ) {
			$result[] = $this->make( $item );
		}

		return $result;
	}

	public function get( $id = null ) {
		$bean = $this->orm->get( $this->table, $id );

		return $this->make( $bean );
	}

	public function make( $bean ) {
		$class = $this->model();

		return new $class( $bean, $this );
	}

	public function find( $conditions = '', $bindings = [] ) {
		$result = [];
		foreach ( $this->orm->find( $this->table, $conditions, $bindings ) as $item ) {
			$result[] = $this->make( $item );
		}

		return $result;
	}

	public function saveAll( array $orm_models ) {
		$items = [];

		foreach ( $orm_models as $orm_model ) {
			$items[] = $this->get_bean( $orm_model );
		}

		$this->orm->saveAll( $items );
	}

	public function get_bean( AbstractOrmModel $orm_model ) {
		$bean = $this->bean_from_mapper( $orm_model );

		return $this->handle_relations( $orm_model, $bean );
	}

	public function handle_relations( AbstractOrmModel $orm_model, $bean ) {
		foreach ( $orm_model->relations() as $key => $relations ) {
			// TODO: make this recursive
			if ( $key === '1_m' ) {
				foreach ( $relations as $relation ) {
					if ( isset( $relation['autosave'] ) && $relation['autosave'] ) {
						$c     = new \ReflectionClass( $relation['model'] );
						$table = $c->getConstant( 'TABLE' );
						// We should save the related tables here
						$callback = $relation['callback'] ?? sprintf( 'get_%s', $table );


						if (!method_exists($orm_model, $callback) && !property_exists($orm_model, $table)) {
							throw new \Exception('You need to specify callback or add public property for relation');
						}

						if (method_exists($orm_model, $callback)) {
							$subitems = $orm_model->$callback();
						} else {
							$subitems = $orm_model->$table;
						}

						$items = [];


						if ( is_array( $subitems ) ) {
							foreach ( $subitems as $item ) {
								$b       = $this->bean_from_mapper( $item );
								$items[] = $this->handle_relations( $item, $b );
							}
						}
						$prop = sprintf( 'xown%sList', $this->camelize( $this->orm->get_table_name( $table ) ) );

						$bean->{$prop} = $items;
					}
				}
			}
		}

		return $bean;
	}


	/**
	 * @param AbstractOrmModel $orm_model
	 *
	 * @return int|mixed|string
	 */
	public function save( $orm_model ) {
		return $this->orm->save( $this->get_bean( $orm_model ) );
	}

	public function bean_from_mapper( AbstractOrmModel $model ) {
		$bean = $this->orm->dispense( $model::TABLE );

		foreach ( $model->own_props() as $key => $prop ) {
			if ($prop['source'] !== 'object') {
				continue;
			}
			if ($key === 'id' && $model->$key === null) {
				continue;
			}

			$bean[ $key ] = $model->$key;
		}

		return $bean;
	}

	public function camelize( $input, $separator = '_' ) {
		return str_replace( $separator, '', ucwords( $input, $separator ) );
	}

	/**
	 * @return string
	 */
	public function get_table(): string {
		return $this->table;
	}

	/**
	 * @return ORM
	 */
	public function get_orm(): ORM {
		return $this->orm;
	}

	protected function resolve_object( $data ) {
		return $data;
	}

	public function delete( $model ) {
		// TODO: IMplement delete method
	}
	/**
	 * @return
	 */
	public function create() {
		return $this->factory( null );
	}
}
