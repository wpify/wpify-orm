<?php

namespace Wpify\Orm\Abstracts;

use PmsDeps\Wpify\Model\Abstracts\AbstractModel;

abstract class AbstractOrmModel extends AbstractModel {
	public $id;
	public function relations() {
		return [];
	}

	public function model_repository(  ) {
		return $this->_repository;
	}
}
