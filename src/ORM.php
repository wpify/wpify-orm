<?php


namespace Wpify\Orm;


use RedBeanPHP\R;

class ORM {
	/** @var R $orm */
	private $orm;
	private $args = [];

	/**
	 * @return bool|void
	 */
	function __construct( $args = [] ) {
		$defaults   = [
			'freeze'      => true,
			'db_name'     => DB_NAME,
			'host'        => DB_HOST,
			'db_user'     => DB_USER,
			'db_password' => DB_PASSWORD,

		];
		$args       = wp_parse_args( $args, $defaults );
		$this->args = $args;
		$this->orm  = new R();

		$this->orm->setup( sprintf( 'mysql:host=%s;dbname=%s', $args['host'], $args['db_name'] ), $args['db_user'], $args['db_password'] );
		if ( $args['freeze'] ) {
			$this->orm::freeze();
		}
	}

	public function get_table_name( $table ) {
		global $wpdb;

		return isset( $this->args['disable_prefix'] ) && $this->args['disable_prefix'] ? $table : $wpdb->prefix . $table;
	}

	public function get_redbean() {
		return $this->orm->getRedBean();
	}

	public function dispense( $table ) {
		return $this->get_redbean()->dispense( $this->get_table_name( $table ) );
	}

	public function saveAll( $beans ) {
		return $this->orm::storeAll( $beans );
	}


	public function save( $bean ) {
		return $this->orm::store( $bean );
	}

	public function get( $table, $id ) {
		return $this->orm::load( $this->get_table_name( $table ), $id );
	}

	public function all( $table ) {
		return $this->orm::findAll( $this->get_table_name( $table ) );
	}

	public function find( $table, $conditions = '', $bindings = [] ) {
		if ( ! empty( $bindings ) ) {
			return $this->orm::find( $this->get_table_name( $table ), $conditions, $bindings );
		} elseif ( $conditions ) {
			return $this->orm::find( $this->get_table_name( $table ), $conditions );
		} else {
			return $this->orm::find( $this->get_table_name( $table ) );
		}
	}


}
